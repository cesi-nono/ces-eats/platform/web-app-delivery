# Setup environment

You need:

- a PC (hihihi)
- Visual Studio Code (or other code editor / IDE)
- Node.js (npm & npx included)
- Docker
- Git

Recommended VSCode extensions:

- GitLens
- npm
- npm Intellisense
- Docker
- Vetur
- Vue 3 Snippets
- Better Comments
- Auto Close Tag
- Auto Rename Tag
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

  > This one is to support linting when formatting page (Ctrl+Shift+I) or saving page while 'Format on save' is enabled (recommended), instead of running `npm run lint` every time. You should enable `format on paste` and `format on save` in VSCode settings, and set Prettier as the `default formatter`.

# Build and run

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run start:dev

# Lint & test
$ npm run lint
$ npm run test

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

_If `npm install` crashes with multiple errors, check that you have Python 3 and Visual C++ build tools installed on your machine. [More info here](https://github.com/nodejs/node-gyp#on-windows)._

For Docker instructions, refer to CESEATS Platform repository's `README.md` file, which explains how to use Docker to build and run the complete platform environment.

# Development

## Project specifications

> This part describes what have been done to generate the project as it is, and the different modules that we use.
> You do NOT need to do this to run the project, it has a reference purpose.

```bash
# command used for project generation
$ npx create-nuxt-app <folder>
```

Options:

- **Project name:** web-app-delivery
- **Programming language:** TypeScript
- **Package manager:** npm
- **UI framework:** Vuetify
- **Nuxt modules:** Axios
- **Linting tools:** ESLint, Prettier & StyleLint
- **Testing framework:** Jest
- **Rendering mode:** Universal (SSR / SSG)
- **Deployment target:** Server (Node.js hosting)
- **Development tools:** None
- **Continuous integration:** None
- **Version control system:** Git

## Dev with Nuxt

The solution is divided in the following list of folders:

- assets
- components
- layouts
- middleware
- pages
- plugins
- static
- store
- test

Each folder contains another `README.md` file for more info.

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
